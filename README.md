# Give Change

## Prerequisite 
* Java 11
* Maven =>3.6

## How to use 
### Generate jar 
```bash
mvn package

```
### Call with Amount = 10
```bash 
java -jar target/test-0.0.1-SNAPSHOT-jar-with-dependencies.jar -a 10 
```