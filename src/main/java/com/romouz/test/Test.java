package com.romouz.test;

import java.util.NoSuchElementException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import com.romouz.test.service.GiveChange;

public class Test {
    private static final HelpFormatter formatter = new HelpFormatter();

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("a", "amount", true, "Amount to give (positive long)");
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine cmd = parser.parse(options, args);
            if (cmd.hasOption("a")) {
                String value = cmd.getOptionValue("a").trim();
                try {
                    System.out.println(GiveChange.getGiveChange()
                            .optimalChange(Long.parseLong(value)).orElseThrow());
                } catch (NoSuchElementException e) {
                    System.err.printf("No Change possible for %s%n", value);
                }
            } else {
                formatter.printHelp("Test", options);
            }
        } catch (MissingArgumentException | NumberFormatException e) {
            System.err.println("Error :" + e.getMessage());
            formatter.printHelp("Test", options);
        } catch (ParseException e) {
            System.err.println("Error :" + e.getMessage());
        }
    }
}
