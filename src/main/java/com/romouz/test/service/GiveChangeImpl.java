package com.romouz.test.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import com.romouz.test.model.Change;
import com.romouz.test.model.Money;
import com.romouz.test.model.MoneyQuantity;

public class GiveChangeImpl implements GiveChange {

    private final SortedSet<Money> moneys = new TreeSet<>();

    private Integer min;

    GiveChangeImpl(Set<Money> moneys) {
        this.moneys.addAll(Objects.requireNonNull(moneys));
        min = this.moneys.last().getValue();
    }

    public Optional<Change> optimalChange(long amount) {
        if (amount < 0) {
            throw new IllegalArgumentException("Amount must be > 0");
        }
        final List<MoneyQuantity> moneyQuantities = new ArrayList<>();
        long remainder = amount;
        for (Money money : moneys) {
            long quantity = remainder / money.getValue();
            long modulos = remainder % money.getValue();
            if ((modulos != 0 && modulos >= min && quantity != 0)
                    || (modulos == 0 && quantity != 0)) {
                remainder = modulos;
                moneyQuantities.add(new MoneyQuantity(money, quantity));
            }
        }
        if (remainder != 0) {
            return Optional.empty();
        }
        return Optional.of(new Change(moneyQuantities));
    }
}
