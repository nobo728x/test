package com.romouz.test.service;

import java.util.Optional;
import java.util.Set;
import com.romouz.test.model.Change;
import com.romouz.test.model.Money;
import com.romouz.test.model.MoneyType;

public interface GiveChange {

    public Optional<Change> optimalChange(long amount);

    static GiveChange getGiveChange() {
        return new GiveChangeImpl(Set.of(new Money(10, MoneyType.BILL),
                new Money(5, MoneyType.BILL), new Money(2, MoneyType.PIECE)));
    }

}