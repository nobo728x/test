package com.romouz.test.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;


@Getter
@EqualsAndHashCode
@AllArgsConstructor
@ToString
public class Money implements Serializable, Comparable<Money> {
    private static final long serialVersionUID = 6077647190959265628L;

    @NonNull
    private Integer value;
    @NonNull
    private MoneyType type;

    @Override
    public int compareTo(Money o) {
        return o.value.compareTo(this.value);
    }
}
