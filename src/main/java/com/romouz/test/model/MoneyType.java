package com.romouz.test.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum MoneyType {

    BILL("Bill"), PIECE("Piece");

    private String description;
}
