package com.romouz.test.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@AllArgsConstructor
@Getter
@ToString
public class MoneyQuantity implements Serializable {

    private static final long serialVersionUID = -7053014369425098138L;

    @NonNull
    private Money money;
    @NonNull
    private Long quantity;

}
