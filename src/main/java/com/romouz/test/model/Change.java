package com.romouz.test.model;

import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString
public class Change implements Serializable {

    private static final long serialVersionUID = 3680642955858987037L;

    private List<MoneyQuantity> moneyQuantities;

}
