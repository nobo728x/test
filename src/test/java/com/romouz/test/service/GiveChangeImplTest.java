package com.romouz.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;
import com.romouz.test.model.Change;
import com.romouz.test.model.Money;
import com.romouz.test.model.MoneyQuantity;
import com.romouz.test.model.MoneyType;

class GiveChangeImplTest {

    @Test
    void testOptimalChange1() {
        GiveChange giveChange = new GiveChangeImpl(Set.of(new Money(10, MoneyType.BILL),
                new Money(5, MoneyType.BILL), new Money(2, MoneyType.PIECE)));
        Optional<Change> change = giveChange.optimalChange(1);
        assertTrue(change.isEmpty());
    }

    @Test
    void testOptimalChange6() {
        GiveChange giveChange = new GiveChangeImpl(Set.of(new Money(10, MoneyType.BILL),
                new Money(5, MoneyType.BILL), new Money(2, MoneyType.PIECE)));
        Optional<Change> change = giveChange.optimalChange(6);
        assertTrue(change.isPresent());
        assertEquals(1, change.get().getMoneyQuantities().size());
        MoneyQuantity moneyQuantity = change.get().getMoneyQuantities().get(0);
        assertEquals(3, moneyQuantity.getQuantity());
        assertEquals(2, moneyQuantity.getMoney().getValue());
    }

    @Test
    void testOptimalChange10() {
        GiveChange giveChange = new GiveChangeImpl(Set.of(new Money(10, MoneyType.BILL),
                new Money(5, MoneyType.BILL), new Money(2, MoneyType.PIECE)));
        Optional<Change> change = giveChange.optimalChange(10);
        assertTrue(change.isPresent());
        assertEquals(1, change.get().getMoneyQuantities().size());
        MoneyQuantity moneyQuantity = change.get().getMoneyQuantities().get(0);
        assertEquals(1, moneyQuantity.getQuantity());
        assertEquals(10, moneyQuantity.getMoney().getValue());
    }

    @Test
    void testOptimalChangeMaxLong() {
        GiveChange giveChange = new GiveChangeImpl(Set.of(new Money(10, MoneyType.BILL),
                new Money(5, MoneyType.BILL), new Money(2, MoneyType.PIECE)));
        Optional<Change> change = giveChange.optimalChange(Long.MAX_VALUE);
        assertTrue(change.isPresent());
        assertEquals(3, change.get().getMoneyQuantities().size());
        MoneyQuantity moneyQuantity = change.get().getMoneyQuantities().get(0);
        assertEquals(922337203685477580L, moneyQuantity.getQuantity());
        assertEquals(10, moneyQuantity.getMoney().getValue());
        moneyQuantity = change.get().getMoneyQuantities().get(1);
        assertEquals(1, moneyQuantity.getQuantity());
        assertEquals(5, moneyQuantity.getMoney().getValue());
        moneyQuantity = change.get().getMoneyQuantities().get(2);
        assertEquals(1, moneyQuantity.getQuantity());
        assertEquals(2, moneyQuantity.getMoney().getValue());
    }

    @Test
    void testOptimalChangeNegative() {
        GiveChange giveChange = new GiveChangeImpl(Set.of(new Money(10, MoneyType.BILL),
                new Money(5, MoneyType.BILL), new Money(2, MoneyType.PIECE)));
        assertThrows(IllegalArgumentException.class, () -> giveChange.optimalChange(-1));
    }

}
